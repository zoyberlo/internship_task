<?php

use App\Http\Controllers\SocialiteController;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/login/google', [SocialiteController::class, 'googleRedirect'])->name('login.google');
Route::get('/login/google/callback', [SocialiteController::class, 'googleLogIn']);


Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/admin_panel', function () {
        return view('admin_panel');
    })->name('admin_panel');
});