<?php 
    use Illuminate\Support\Facades\DB;
?>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Admin panel') }}
        </h2>
<!-- ................................................................................... 
        <div class="flex justify-between h-16 ">
            <div class="flex">
                <!-- Navigation Links 
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                        {{ __('Users list') }}
                    </x-nav-link>
                </div>
                    <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                        <x-nav-link :href="route('admin_panel')" :active="request()->routeIs('admin_panel')">
                            {{ __('Create new user') }}
                        </x-nav-link>
                    </div>
                </div>           
            </div>
        </div>
    ................................................................................... -->
    </x-slot>
    
    <!--  <link rel="stylesheet" href="table.css">  -->

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">            
                <div class="p-6 bg-white border-b border-gray-200">

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <!-- Name -->
                    <div>
                        <x-label for="name" :value="__('Name')" />

                        <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                    </div>

                    <!-- Email Address -->
                    <div class="mt-4">
                        <x-label for="email" :value="__('Email')" />

                        <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                    </div>

                    <!-- Password -->
                    <div class="mt-4">
                        <x-label for="password" :value="__('Password')" />

                        <x-input id="password" class="block mt-1 w-full"
                                        type="password"
                                        name="password"
                                        required autocomplete="new-password" />
                    </div>
                <!--
                    <div class="mt-4">
                        <x-label for="number" :value="__('role id')" />

                        <x-input id="password" class="block mt-1 w-full"
                                        type="password"
                                        name="password"
                                        required autocomplete="new-password" />
                    </div>
                -->
                    <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                            {{ __('Register') }}
                        </x-button>
                    </div>

                </form>

                    <div class="p-6">

                        <style>
                            .table {
                                width: 100%;
                                margin-bottom: 20px;
                                border: 1px solid #dddddd;
                                border-collapse: collapse; 
                            }
                            .table th {
                                font-weight: bold;
                                padding: 5px;
                                background: #efefef;
                                border: 1px solid #dddddd;
                            }
                            .table td {
                                border: 1px solid #dddddd;
                                padding: 5px;
                            }
                        </style>
                        
                        <table class="table">
                            <thead>
                                <tr>
                                    <th> id </th>
                                    <th> name </th>
                                    <th> email </th>
                                    <th> created at </th>
                                    <th> role id </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                //$titles = DB::table('users')->pluck('title');
                                $mainInformation = DB::table('users')
                                    ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                                    ->select('users.id',
                                            'users.name', 
                                            'users.email', 
                                            'users.created_at', 
                                            'model_has_roles.role_id')
                                    ->get();
                                //var_dump($mainInformation);
                                foreach($mainInformation as $user){
                                // конец php кода
                                    ?>

                                    <tr>
                                        <td><?= $user->id ?></td>
                                        <td><?= $user->name ?></td>
                                        <td><?= $user->email ?></td>
                                        <td><?= $user->created_at ?></td>
                                        <td><?= $user->role_id ?></td>
                                    </tr>
                                    <?php 
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">