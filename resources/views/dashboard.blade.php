<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <form method="POST" action="{{ route('login') }}">
                    <!-- Password -->
                    <div class="mt-4; align-items: center">
                        <x-label for="password" :value="__('Password')" />

                        <x-input id="password" class="block mt-1"
                                        type="password"
                                        name="password" />
                    </div>
                    <div class="display: flex; align-items: center;">
                        <x-button>
                            {{ __('Log in') }}
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>            
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    You're logged in!
                </div>
            </div>
        </div>
    </div>
    <div>
        <form method="POST" action="{{ route('login') }}">
            <input type="tel">
            <input type="submit">
        </form>
    </div>
</x-app-layout>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
