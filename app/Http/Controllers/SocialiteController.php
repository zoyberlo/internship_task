<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Exception;

class SocialiteController extends Controller
{
    public function googleRedirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function googleLogIn()
    {
        try {
            $user = Socialite::driver('google')->user();
            //поиск пользователя в базе данных
            $isUser = User::where('google_id', $user->id)->first();

            //Проверка регистрации пользователя
            if ($isUser) {
                Auth::login($isUser);
                return redirect('/dashboard');
            } else {
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'password' => encrypt('user'),
                ]);

                event(new Registered($createUser)); // Разобраться!!!!

                $createUser->assignRole('user'); // изначальная роль пользователя

                Auth::login($createUser);

                return redirect('/dashboard');
            }

        } catch (Exception $exception) {
            //dd($exception->getMessage());
            return Socialite::driver('google')->redirect();
        }
    }
}
